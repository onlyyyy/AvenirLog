
## Simple Example  一个简单的示例

```js
//a simple example for AvenirLog 
//本程序提供一个简单的示例
const AvenirLog = require('avenir-log');
async function main() {
    let log = new AvenirLog('test','./log');   //logFileName,logOutPath,1-日志文件名，2-日志文件的存放目录
    log.noDebug()
    log.noConsoleLog();
    global.log = log;       //For direct use 这样之后可以在其他文件直接使用

    //5 log types 五种日志记录类型
    log.debug('write log');
    log.error('write error');
    log.warn("write warn");
    log.log('write object',{code:200,data:'error'}); 
    log.info('write info');       
}

main();
```

-----

### Results

in ./log/test_20201127.log

```
[debug] 2020-11-27 11:58:43 : write log
[error] 2020-11-27 11:58:43 : write error
[warn] 2020-11-27 11:58:43 : write warn
[log] 2020-11-27 11:58:43 : write object {"code":200,"data":"error"}
```


## APIS

- 1.`debug(...args)`  usage same as console.log()  和console.log()一样，逗号分隔参数
- 2.`error(...args)`  omited
- 3.`info(...args)`   omited
- 4.`log(...args)`    omited
- 5.`warn(...args)`   omited
- 6.`init(fileName,filePath)`  same as constructor `let log = new Logger('test','./log'); ` 和构造函数的用法一样
- 7.`noDebug()`      use this function once to avoid log debugs to file，Just like a switch调用一次这个函数，就不会写入debug到日志里面了，和开关一样。
- 8.`noConsoleLog` use this function once to avoid console.log("your logs")，Just like a switch调用一次这个函数，就不会把日志同时输出一遍了，和开关一样。
