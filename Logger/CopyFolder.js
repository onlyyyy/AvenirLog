//
var fs = require('fs');
var path = require('path');
var walk = require('walk');


//对目录进行操作
class CopyFolder {
    constructor() {
        
    }

    fileExists(filePath) {
        var promise = new Promise((resolve, reject) => {
            fs.exists(filePath, (exists) => {
                resolve(exists);
            });
        });
        return promise;
    }
    
    copyFile(srcPath, tarPath) {
        var promise = new Promise((resolve, reject) => {
            var rs = fs.createReadStream(srcPath);
            rs.on('error', function(err) {
                if (err) {
                    console.log('read error', srcPath)
                    reject(err);
                }
            });
            
            var ws = fs.createWriteStream(tarPath);
            ws.on('error', function(err) {
                if (err) {
                    console.log('write error', tarPath);
                    reject(err);
                }
            });
            
            ws.on('finish', function() {
                resolve();
            });
            
            rs.pipe(ws);
        });
        
        return promise;
    }

    deleteFile(filePath) {
        var promise = new Promise((resolve, reject) => {
            fs.exists(filePath, (exists) => {
                if (exists) {
                    fs.unlink(filePath, (err) => {
                        if (err) {
                            console.log(err);
                        }
                        resolve();
                    });
                } else {
                    resolve();
                }
            });
        });

        return promise;
    }

    rmdir(dirname) {
        var promise = new Promise((resolve, reject) => {
            fs.exists(dirname, (exists) => {
                if (exists) {
                    fs.rmdir(dirname, (err) => {
                        if (err) {
                            console.log(err);
                        }
                        resolve();
                    });
                } else {
                    resolve();
                }
            });
        });

        return promise;
    }

    mkdir(dirname) {
        var promise = new Promise((resolve, reject) => {
            fs.exists(dirname, (exists) => {
                if (!exists) {
                    fs.mkdir(dirname, (err) => {
                        if (err) {
                            console.log(err);
                        }
                        resolve();
                    });
                } else {
                    resolve();
                }
            });
        });

        return promise;
    }

    rename(oldName, newName) {
        let promise = new Promise((resolve, reject) => {
            fs.rename(oldName, newName, (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });

        return promise;
    }
    
    stat(filePath) {
        var promise = new Promise((resolve, reject) => {
            fs.stat(filePath, (err, stats) => {
                if (err) {
                    console.log(err);
                    reject(err);
                }
            
                resolve(stats);
            });
        });

        return promise;
    }

    readFile(filePath, options) {
        var promise = new Promise((resolve, reject) => {
            fs.readFile(filePath, options, function(err, data) {
                if (err) {
                    console.log(err);
                    reject(err);
                }
            
                resolve(data);
            });
        });

        return promise;
    }

    writeFile(filePath, content, options) {
        var promise = new Promise((resolve, reject) => {
            fs.writeFile(filePath, content, options, function(err) {
                if (err) {
                    console.log(err);
                    reject(err);
                }
            
                resolve();
            });
        });

        return promise;
    }
    
    walkFolder(dirname, option) {
        let myOption = {
            recursive: true 
        };
        if (option) {
            for (let key in option) {
                myOption[key] = option[key];
            }
        }
        
        var promise = new Promise((resolve, reject) => {
            var walker = walk.walk(dirname, { followLinks: false });
            var fileList = [];
            var dirList = [];
        
            walker.on('file', (roots, stat, next) => {
                if (myOption.recursive) {
                    fileList.push(path.join(roots, stat.name));
                } else {
                    if (roots == dirname) {
                        fileList.push(path.join(roots, stat.name));
                    }
                }
                next();
            });
         
            walker.on('directory', (roots, stat, next) => {
                if (myOption.recursive) {
                    dirList.push(path.join(roots, stat.name));
                } else {
                    if (roots == dirname) {
                        dirList.push(path.join(roots, stat.name));
                    }
                }
                next();
            });

            walker.on('end', () => {
                resolve({
                    fileList : fileList,
                    dirList : dirList
                });
            });
        });

        return promise;   
    }
    
    
    
    async copyFolder(srcDir, tarDir, filter) {

        var src = path.normalize(srcDir);
        var tar = path.normalize(tarDir);

        console.log('src', src);
        console.log('tar', tar);
        

        //var newFolderPath = path.join(tar, path.basename(src));
        //console.log("newFolderPath = " + newFolderPath);
        //await this.mkdir(newFolderPath);
        
        var dirs = await this.walkFolder(src);
        console.log(dirs);
        
        for (var i = 0; i < dirs.dirList.length; i++) {
            //console.log(dirs.dirList[i]);
            //console.log(src);
            //console.log(newFolderPath);
            var dirname = dirs.dirList[i].replace(src, tar);
            console.log(dirname);
            await this.mkdir(dirname);
        }
        
        for (var i = 0; i < dirs.fileList.length; i++) {
            //var newFilePath = path.join(newFolderPath, path.basename(dirs.fileList[i]));
            
            var filename = dirs.fileList[i].replace(src, tar);
            //console.log(filename);

            if (filter) {
                if (filter.test(path.basename(filename))) {
                    await this.copyFile(dirs.fileList[i], filename);
                    console.log(filename);
                }
            } else {
                await this.copyFile(dirs.fileList[i], filename);
                console.log(filename);
            }
        }
    }

    async removeFolder(srcDir) {
        var src = path.normalize(srcDir);
        
        console.log('src', src);
                
        var dirs = await this.walkFolder(src);
        console.log(dirs);
        
        for (var i = 0; i < dirs.fileList.length; i++) {            
            var filename = dirs.fileList[i];
            console.log(filename);
            await this.deleteFile(filename);
        }

        for (var i = dirs.dirList.length - 1; i >= 0; i--) {
            var dirname = dirs.dirList[i];
            console.log(dirname);
            await this.rmdir(dirname);
        }

        await this.rmdir(src);
    }
}


module.exports = CopyFolder;
