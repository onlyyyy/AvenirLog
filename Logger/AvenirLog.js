const fs = require('fs');
const path =require('path');
const moment = require('moment')
const date = new Date();

class AvenirLog {

    //调用构造函数的时候赋值
    constructor(name,path) {
        let YYYYMMDD = moment().format("YYYYMMDD");
        this.fileName = name + '_' + YYYYMMDD + '.log' || '';
        this.path = path || '';
        this.isDebug = true;
        this.isOutPut = true;
    }

    padZero(num, len) {
        return (Array(len).join('0') + num).slice(-len);
    }

    noDebug() {
        this.isDebug = false;
    }

    noConsoleLog() {
        this.isOutPut = false;
    }

    init(fileName,filePath) {
        let YYYYMMDD = moment().format("YYYYMMDD");
        this.fileName = fileName + '_' + YYYYMMDD + '.log' || '';
        this.path = filePath || '';
    }

    //判断这个对象是否是Json 是Json就转字符串 不然写入日志全是[obejct object]
    jsonCheck(text) {
        if(typeof text == 'object') {
            try {
                let a = JSON.stringify(text);
                return a;
            } catch(error){
                return text
            }
        } 
        return text ;   //不是对象没必要
    }

    writeLog(text) {
        //顺便输出
        if(!fs.existsSync(this.path)) {
            fs.mkdirSync(this.path,{recursive:true});
        }
        let fileName = path.join(this.path,this.fileName);
        !this.isOutPut || console.log(text);
        try {
            
            fs.appendFile(fileName,text,null,function(error){
                if(error) {
                    throw(`in appendFile  error = ${error}`);
                }
            });            //同步增加效率
        } catch(error) {
            !this.isOutPut || console.log("error = ",error);
        }
        
    }

    parseText(args,type) {
        let logs = null;
        //处理 拼接日志信息 
        const YYYYMMDDHHmmss = moment().format("YYYY-MM-DD HH:mm:ss");
        this.padZero(date.getHours(),2)+":" + this.padZero(date.getMinutes(),2) + ":" + this.padZero(date.getSeconds(),2);
        logs = `[${type}] ` + YYYYMMDDHHmmss + " : ";
        if (typeof args != 'object' || args.length == 0) {
            logs += 'lack of log texts';
        } else {
            for (let i = 0; i < args.length; i++) {
                if(i == 0 ){
                    logs += this.jsonCheck(args[i]);
                } else {
                    logs += ' ' + this.jsonCheck(args[i]);
                }
                
            }
        }
        logs += '\n';       //最后一条得换行
        return logs;
    }

    debug(...args) {

        //关闭Debug
        if(!this.isDebug) {
            !this.isOutPut || console.log(this.parseText(args,'debug'));
            return;
        }
        if (!this.fileName) {
            throw ("lack of log filename,please use constructor or set(fileName)");
        }
        if (!this.setting) {
            //暂时不做处理
        }
        
        this.writeLog(this.parseText(args,'debug'));
    }

    error(...args) {
        if (!this.fileName) {
            throw ("lack of log filename,please use constructor or set(fileName)");
        }
        if (!this.setting) {
            //暂时不做处理
        }
        
        this.writeLog(this.parseText(args,'error'));
    }

    warn(...args) {
        if (!this.fileName) {
            throw ("lack of log filename,please use constructor or set(fileName)");
        }
        if (!this.setting) {
            //暂时不做处理
        }
        
        this.writeLog(this.parseText(args,'warn'));
    }

    info(...args) {
        if (!this.fileName) {
            throw ("lack of log filename,please use constructor or set(fileName)");
        }
        if (!this.setting) {
            //暂时不做处理
        }
        
        this.writeLog(this.parseText(args,'info'));
    }


    log(...args) {
        if (!this.fileName) {
            throw ("lack of log filename,please use constructor or set(fileName)");
        }
        if (!this.setting) {
            //暂时不做处理
        }
        
        this.writeLog(this.parseText(args,'log'));
    }



}

exports = module.exports = AvenirLog;
