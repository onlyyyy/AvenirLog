exports.version = 'AvenirLog V1.0.5';

exports.error = {
    PARAMS_ERROR: "params error",
    FILE_NOT_EXIST: "the file is not exist",
    PATH_NOT_EXIST: "the dir is not exist",
    FILE_PATH_LACK : "lack of file or path",
    FILE_PATH_REPEAT:"file or path repeat",
    NOT_EXIST:"the file or path is not exist",
    UNKNOWN_ERROR:"unknown params error",
    NOT_PATH:"params not a path",
    NOT_FILE:"params not a file",
    INVALID_LOG_FILE:"not a Avenir log file",
    LACK_OF_PARAMS:"lack of params",
    WORK_TYPE_ERROR:"work log type error",
}