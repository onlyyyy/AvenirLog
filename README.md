# Logger

#### 介绍
Node.js日志库

#### 日志生成使用示例

```js
//本程序提供一个Logger的使用示例
const Logger = require('avenirlog');
async function main() {
    let log = new Logger('test','./log');   //1-日志文件名，2-日志文件的存放目录
    global.log = log;       //这样之后可以在其他文件直接使用

    //四种日志记录类型
    log.debug('写入日志');
    log.error('写入错误信息');
    log.warn("出现警告");
    log.log('写入对象',{code:200,data:'error'});        
}

main();
```


#### 日志收集部分AvenirLog所支持的命令

- -V -v --version 
获取AvenirLog的版本，必须是第一个参数，且之后的参数不会再被处理

- -d 
设置这次工作的目的，获取日志的类型，默认error，多种类型的日志用/分割 可选参数e-error w-warn  i-info d-debug 
如：-d e 、-d "e/d/i" 获取这三种日志  排序方式为e/w/i/d

- -p
设定AvenirLog本次需要工作的路径 但需要注意AvenirLog不会进行递归操作
如 -p "./log"

- -f
设定AvenirLog本次处理的文件
如 -f "./log/1.txt"

- fileName
智能选项，必须是第一个参数，设定AvenirLog本次处理的文件，AL会自动判断是相对路径还是绝对路径。
如 "./log/1.txt"

- -t -j
-t：将结果写成文本文件
-j:将结果写成Json文件（默认）
后面可以增加文件名，默认根据当前时间和路径生成文件

- -nohup
只生成文件而不输出信息

- -g 
获取并分析AvenirLog产生的日志分析结果，必须是第一个参数 必须是一个文件
如： -g "./log/AvenirLog_2020112617000.log" 

- -w 
设置AvenirLog的输出路径 默认为"/AvenirLog"

- -wf
设置AvenirLog的输出文件  待定功能
 
-------
    注：1.AvenirLog不会对输入的参数做特别强逻辑的校验，比如先输入-f 后又输入-p ，则之后的参数会被忽略。
    2.所有的路径应尽可能用绝对路径，以避免跨平台产生的问题

