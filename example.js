//a simple example for AvenirLog 
//本程序提供一个简单的示例
const AvenirLog = require('avenir-log');
async function main() {
    let log = new AvenirLog('test','./log');   //logFileName,logOutPath,1-日志文件名，2-日志文件的存放目录
    log.noDebug()
    log.noConsoleLog();
    global.log = log;       //For direct use 这样之后可以在其他文件直接使用

    //4 log types 四种日志记录类型
    log.debug('write log');
    log.error('write error');
    log.warn("write warn");
    log.log('write object',{code:200,data:'error'});        
}

main();