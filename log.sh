#提供在Linux下快速查看日志的方式，但实际需要根据系统情况进行修改

#!/bin/bash
cd ~;

#是否有alias文件，以执行alias命令
source ~/.bash_alias 
shopt -s  expand_aliases

#系统当前时间 脚本是用来查看每天最新的日志的，根据日志格式修改Ymd
time1=$(date +"%Y%m%d")
echo $time1

#获取传入的参数
MgrName=$1
echo $MgrName
type=$2
echo $type

#alias命令，切换到日志目录下
cdl;

#根据实际情况配置日志文件格式
if [ "$MgrName" = "home" ];then
FileName="Home_"$time1".log";

elif [ "$MgrName" = "trans" ];then
FileName="HM_"$time1"_sc_transMgr.log"

else
echo "params error"
exit
fi
echo $FileName

if [ "$type" = "-f" ];then
tail -f $FileName
else
vim $FileName
fi