//实现日志收集分析的主函数
const CONSTANTS = require('./Logger/constants');
const fs = require('fs');
const path = require('path');

const readline = require('n-readlines');
const CopyFolder = require('./Logger/CopyFolder');
const cf = new CopyFolder();
const moment = require('moment');
let fileName = null;
let filePath = null;
let isJson = false;     //默认不输出Json
let nohup = false;
let hasWork = false; //是否已经有了工作的目标
let workPath = './AvenirLog/';

let errors = true, debugs = false, infos = false, warns = false, logs = false;     //四种日志类型的处理

async function main() {
    var args = process.argv.splice(2);  //获取运行时参数
    for (let i = 0; i < args.length; i++) {
        //-v -V --version必须是第一个，且后面的参数不会被处理
        i = await parseCommand(args, i)

    }
    if (!fileName && !filePath) {
        throwError(CONSTANTS.error.FILE_PATH_LACK);
    }
    if (fileName && filePath) {
        throwError(CONSTANTS.error.FILE_PATH_REPEAT);
    }
    log("要处理的文件或目录为", fileName || filePath);
    fileName ? await parseLog(fileName) : await parseLogByPath(filePath);

}

//解析参数的函数，必须是传入args和索引
async function parseCommand(args, i) {
    switch (args[i]) {
        case '-v':
        case '-V':
        case '--version':
            if (i === 0) {
                throwError(CONSTANTS.version,0);
            }
        //不需要break
        case '-f':
            if (hasWork) {
                return;
            }
            fileName = args[i + 1];
            i++;
            if (!fs.existsSync(fileName)) {
                throwError(CONSTANTS.error.FILE_NOT_EXIST);
            }
            if (!fs.lstatSync(fileName).isFile()) {
                throwError(CONSTANTS.error.NOT_FILE);
            }
            break;
        case '-p':
            if (hasWork) {
                return;
            }
            filePath = args[i + 1];
            i++;
            if (!fs.existsSync(filePath)) {
                throwError(CONSTANTS.error.FILE_NOT_EXIST);
            }
            if (!fs.lstatSync(filePath).isDirectory()) {
                throwError(CONSTANTS.error.NOT_PATH);
            }
            break;
        case '-t':
            isJson = false;
            break;
        case '-j':
            isJson = true;
            break;
        case '-nohup':
            nohup = true;
            break;
        case '-w':
            {
                let tempPath = args[i + 1];
                if (!tempPath) {
                    throwError(CONSTANTS.error.LACK_OF_PARAMS);
                }
                i++;
                if (!fs.existsSync(tempPath)) {
                    fs.mkdirSync(tempPath,{recursive:true});
                } else if (!fs.lstatSync(tempPath).isDirectory()) {
                    console.log(CONSTANTS.error.NOT_FILE);
                }
                workPath = tempPath;
                break;
            }
        case '-d':
            {
                let filter = args[i + 1];
                i++;
                let arr = filter.split('/');
                log("指定需要的日志-d = ",arr);
                errors = false;
                for (let j = 0; j < arr.length; j++) {
                    if (arr[j] == 'e' || arr[j] ==  'error') {
                        errors = true;
                    }
                    else if (arr[j] == 'd' ||arr[j] ==  'debug') {
                        debugs = true;
                    }
                    else if (arr[j] == 'i' ||arr[j] ==  'info') {
                        infos = true;
                    }
                    else if (arr[j] == 'w' ||arr[j] ==  'warn') {
                        warns = true;
                    }
                    else if (arr[j] == 'l' || arr[j] == 'log') {
                        logs = true;
                    }
                    else {
                        log(CONSTANTS.error.WORK_TYPE_ERROR);
                        //但是不需要报错
                    }
                }
                console.log("目前的日志输出情况",errors,debugs,infos,logs,warns);
                errors || infos || warns || debugs || logs || (errors = true);     //没设置任何参数则输出error

                break;

            }
        case '-g':
            //解析AvenirLog的输出文件
            break;

        default:
            log("默认参数", args[i]);
            //可以是文件名
            if (hasWork) {
                return;
            }
            if (!fs.existsSync(args[i])) {
                throwError(CONSTANTS.error.NOT_EXIST);
            }
            let stat = fs.lstatSync(args[i]);
            if (stat.isDirectory()) {
                filePath = args[i];
                log("set raw filePath to ", filePath);
            }
            else if (stat.isFile()) {
                fileName = args[i];
                log("set raw fileName to ", fileName);

            } else {
                throwError(`${args[i]} ${CONSTANTS.error.PARAMS_ERROR}`);
            }
            break;
    }
    return i;
}

function exit(returnCode) {
    process.exit(returnCode);
}

//处理要分析的日志文件
async function parseLog(fileName) {
    const liner = new readline(fileName);


    //初始化统计
    let sum = {
        error: 0,
        warn: 0,
        debug: 0,
        info: 0,
        log: 0,
    };
    if (!fs.existsSync(workPath)) {
        fs.mkdirSync(workPath,{recursive:true});
    }
    let workFileName = 'AvenirLog_' + moment().format("YYYYMMDDHHmmss") + '_For_' + path.basename(fileName);
    workFileName = path.join(workPath + workFileName);
    log("输出文件路径为 ", workFileName);
    isJson ? toJsonFile(workFileName, liner, fileName, sum) : toTxtFile(workFileName, liner, fileName, sum);


}

//根据路径 处理要分析的日志文件 待开发
async function parseLogByPath(filePath) {

    let list = await cf.walkFolder(filePath);
    log("list = ", list);
    for (let i = 0; i < list.fileLists.length; i++) {

    }

}

async function throwError(error, type) {
    if(type == undefined) {
        type = 1;
    }
    console.log(error);
    console.log("exit with code ", type);
    exit(type);
}

async function toTxtFile(workFileName, liner, rawFileName, sum) {
    fs.appendFileSync(workFileName, CONSTANTS.version + " " + moment().format("YYYY-MM-DD HH:mm:ss") + '\n');
    fs.appendFileSync(workFileName, `raw File Name = ${rawFileName}\n`);


    let line;
    while (line = liner.next()) {
        let array = line.toString('utf-8');
        let temp = array.split(' ');
        //空行跳过但不报错，因为AvenirLog的日志文件最后一行是空行
        if (temp.length == 0) {
            continue;
        }

        //符合要求的文件提取出来
        if (await includes(temp[0], sum)) {
            fs.appendFileSync(workFileName, `${array}\n`);
        } else {
            continue;
        }
    }
    !errors || fs.appendFileSync(workFileName, `${sum.error} errors `);
    !warns || fs.appendFileSync(workFileName, `${sum.warn} warns `);
    !infos || fs.appendFileSync(workFileName, `${sum.info} infos `);
    !logs || fs.appendFileSync(workFileName, `${sum.log} logs `);
    !debugs || fs.appendFileSync(workFileName, `${sum.debug} debugs `);
    fs.appendFileSync(workFileName, '\n');
    fs.appendFileSync(workFileName, `thank you for using AvenirLog!\n`);
    log("AvenirLog处理成功 ",sum);
}

async function toJsonFile(workFileName, liner, rawFileName) {
    let fileJson = {}
    fileJson.createTime = moment().format("YYYY-MM-DD HH:mm:ss");

}

async function includes(text, sum) {
    text == '[error]' || text == '[debug]' || text == '[info]' || text == '[warn]' || text == '[log]' || throwError(CONSTANTS.error.INVALID_LOG_FILE); //说明不是AvenirLog的文件
    if ((text == '[error]') && errors) {
        sum.error++;
        return true;
    }
    else if ((text == '[debug]') && debugs) {
        sum.debug++;
        return true;
    }
    else if ((text == '[info]') && infos) {
        sum.info++;
        return true;
    }
    else if ((text == '[warn]') && warns) {
        sum.warn++;
        return true;
    }
    else if ((text == '[log]') && logs) {
        sum.log++;
        return true;
    }
    return false;

}


// 根据nohup参数决定是否要输出日志
function log(...log) {
    if (!nohup) {
        console.log(...log);
    }
}



main();